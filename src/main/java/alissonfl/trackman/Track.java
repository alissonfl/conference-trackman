package alissonfl.trackman;

import java.util.ArrayList;
import java.util.List;

public class Track {
    private List<TimeSlot> timeSlots;

    public Track() {
        timeSlots = new ArrayList<TimeSlot>();
    }

    public void addSlot(TimeSlot timeSlot) {
        timeSlots.add(timeSlot);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (TimeSlot timeSlot : timeSlots) {
            str.append(timeSlot);
        }
        return str.toString();
    }
}
