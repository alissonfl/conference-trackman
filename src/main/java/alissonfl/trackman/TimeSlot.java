package alissonfl.trackman;

import java.util.List;
import java.util.ArrayList;

import alissonfl.trackman.util.Time;

import static alissonfl.trackman.util.Consts.LINE_BREAK;

public class TimeSlot {
    private List<Event> eventList;
    private TimeSlot reservedTimeSlot;
    private int startTime;
    public int remainingTime;
    
    public TimeSlot(int duration, int startTime) {
        this.remainingTime = duration;
        this.startTime = startTime;
        
        eventList = new ArrayList<Event>();
    }

    public void addEvent(Event event) {
        if (remainingTime < event.getLengthInMinutes()) {
            throw new IllegalStateException("Not enough room in this time slot to fit the event: '" + event.getName() + "'");
        }
        
        eventList.add(event);
        remainingTime -= event.getLengthInMinutes();
    }

    public boolean hasRoomFor(Event event) {
        return remainingTime >= event.getLengthInMinutes();
    }

    public void addReservedTimeSlot(TimeSlot timeSlot) {
        this.reservedTimeSlot = timeSlot;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        int nextEventStartTime = addEventsPlan(eventList, startTime, str);
        
        if (reservedTimeSlot != null) {
            int reservedStartTime = reservedTimeSlot.startTime;
            
            if (nextEventStartTime > reservedTimeSlot.startTime) {
                reservedStartTime = nextEventStartTime;
            }
            
            nextEventStartTime = addEventsPlan(reservedTimeSlot.eventList, reservedStartTime, str);
        }
        
        return str.toString();
    }

    private int addEventsPlan(List<Event> events, int startTime, StringBuilder str) {
        int nextEventStartTime = startTime;
        
        for (Event event : events) {
            str.append(Time.minutesToTimeString(nextEventStartTime) + " " + event + LINE_BREAK);
            nextEventStartTime += event.getLengthInMinutes();
        }

        return nextEventStartTime;
    }
}
