package alissonfl.trackman;

public enum TimeUnitEnum {
    MINUTES(1, "min"), 
    LIGHTENING(5, "lightning");

    private int factor;
    private String stringRepresentation;

    private TimeUnitEnum(int factor, String stringRepresentation) {
        this.factor = factor;
        this.stringRepresentation = stringRepresentation;
    }

    public int inMinutes(int duration) {
        return factor * duration;
    }

    @Override
    public String toString() {
        return stringRepresentation;
    }
}
