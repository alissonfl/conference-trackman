package alissonfl.trackman;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import alissonfl.trackman.util.Logger;

import static alissonfl.trackman.util.Consts.LINE_BREAK;

public class ConferencePlannerMain {
    public static void main(String[] args) {
    	Logger logger = Logger.getLogger();
        
        if (args.length < 1) {
            logger.fatal("You need to pass the file with events as parameter!");
            System.exit(1);
        }
        
        try {
        	File inFile = new File(args[0]);
            BufferedReader buffReader = new BufferedReader(new FileReader(inFile));
            ConferencePlanner confPlanner = new ConferencePlanner();
            Conference conference = confPlanner.plan(buffReader);
            StringBuilder output = new StringBuilder();
            int warnCount = confPlanner.getWarnCount();
            
            //Output planning
            if (conference == null || conference.toString().length() == 0) {
            	output.append("No planning was generated.");
			} else {
	            output.append("Conference planning based on ")
	            	.append(args[0]).append(":")
	            	.append(LINE_BREAK)
	            	.append(conference).append(LINE_BREAK);
	            
	            //Footer with planning results
	            output.append("The conference was ");
	            if (warnCount > 0) {
	            	output.append("planned with " + warnCount + " warning" + (warnCount > 1 ? "s" : "") + ".");
				} else {
					output.append("successfully planned!");
				}
			}
                        
            logger.log(conference.toString());
            
        } catch (IOException e) {
            logger.fatal("Error parsing input file");
            System.exit(1);
        }
    }
}
