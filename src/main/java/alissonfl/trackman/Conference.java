package alissonfl.trackman;

import java.util.List;
import java.util.ArrayList;

import static alissonfl.trackman.util.Consts.LINE_BREAK;

public class Conference {
    private List<Track> trackList;

    public Conference() {
        trackList = new ArrayList<Track>();
    }

    public void addTrack(Track track) {
        trackList.add(track);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < trackList.size(); i++) {
            sb.append(LINE_BREAK)
            	.append("Track " + (i+1) + ":" + LINE_BREAK)
            	.append(trackList.get(i));
        }
        
        return sb.toString();
    }
}
