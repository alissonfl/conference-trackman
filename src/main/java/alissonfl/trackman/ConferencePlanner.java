package alissonfl.trackman;

import static alissonfl.trackman.util.Consts.BEGIN_TIME;
import static alissonfl.trackman.util.Consts.AM_LENGTH;
import static alissonfl.trackman.util.Consts.PM_LENGTH;
import static alissonfl.trackman.util.Consts.LUNCH_LENGTH;
import static alissonfl.trackman.util.Consts.LUNCH_BEGIN_TIME;
import static alissonfl.trackman.util.Consts.MAX_EVENT_LENGTH;
import static alissonfl.trackman.util.Consts.NETW_LENGTH;
import static alissonfl.trackman.util.Consts.NETW_MIN_BEGIN_TIME;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alissonfl.trackman.util.Logger;

public final class ConferencePlanner {
    public ConferencePlanner() {}
    
    private int warnCount = 0;
	private static Logger logger = Logger.getLogger();

    public Conference plan(BufferedReader input) throws IOException {
        List<Event> eventList = new ArrayList<Event>();
        
        for (String line; (line = input.readLine()) != null;) {
            line = line.trim();
            
            Event event = parseLine(line);
            
            if (event == null) {
            	warnCount++;
                continue;
            }
            
            eventList.add(event);
        }

        Conference conference = new Conference();
        
        while (eventList.size() != 0) {
            TimeSlot amTimeSlot = new TimeSlot(AM_LENGTH, BEGIN_TIME);
            TimeSlot pmTimeSlot = new TimeSlot(PM_LENGTH, LUNCH_BEGIN_TIME + LUNCH_LENGTH);
            TimeSlot lunchTimeSlot = new TimeSlot(LUNCH_LENGTH, LUNCH_BEGIN_TIME);
            
            setTimeSlotEvents(amTimeSlot, eventList);
            
            lunchTimeSlot.addEvent(new Event("Lunch", LUNCH_LENGTH, TimeUnitEnum.MINUTES));
            
            setTimeSlotEvents(pmTimeSlot, eventList);
            
            Event networkingEvent = new Event("Networking Event", NETW_LENGTH, TimeUnitEnum.MINUTES);
            TimeSlot networkingTimeSlot = new TimeSlot(networkingEvent.getLengthInMinutes(), NETW_MIN_BEGIN_TIME);
            
            networkingTimeSlot.addEvent(networkingEvent);
            pmTimeSlot.addReservedTimeSlot(networkingTimeSlot);
            
            Track track = new Track();
            track.addSlot(amTimeSlot);
            track.addSlot(lunchTimeSlot);
            track.addSlot(pmTimeSlot);
            
            conference.addTrack(track);
        }

        return conference;
    }

    private static void setTimeSlotEvents(TimeSlot timeSlot, List<Event> events) {
        for (Iterator<Event> iter = events.iterator(); iter.hasNext();) {
            Event event = iter.next();
            
            if (timeSlot.hasRoomFor(event)) {
                timeSlot.addEvent(event);
                iter.remove();
            }
        }
    }

    /**
     * Parse one line at time to identify Event name, duration and time unit.
     * 
     * @param line String
     * @return {@link Event}
     */
    private static Event parseLine(String line) {
        if (line.length() == 0) {
        	logger.warn("Empty line was skipped.");
            return null;
        }

        Pattern linePattern = Pattern.compile("^(.+)\\s(\\d+)?\\s?((min)|(lightning))$");
        Matcher match = linePattern.matcher(line);
        if (match.find() == false) {
            logger.warn("Invalid line: " + line);
            return null;
        }

        TimeUnitEnum unit;
        if (match.group(3).equalsIgnoreCase("min")) {
            unit = TimeUnitEnum.MINUTES;
        } else {
            unit = TimeUnitEnum.LIGHTENING;
        }

        String name = match.group(1);
        String durationInString = match.group(2);
        
        if (durationInString == null) {
            durationInString = "1";
        }
        
        int duration = Integer.parseInt(durationInString);

        Event event = new Event(name, duration, unit);
        
        if (event.getLengthInMinutes() > MAX_EVENT_LENGTH) {
            logger.warn("Event '" + name + "' does not fit in any period. Maximum duration is " 
            		+ MAX_EVENT_LENGTH + " minutes. Skipping this event.");
            return null;
        }

        return event;
    }
    
    public int getWarnCount() {
		return warnCount;
	}
    
}
