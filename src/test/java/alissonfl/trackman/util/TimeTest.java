package alissonfl.trackman.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {
    @Test
    public void testMinutesToTimeString() {
        assertEquals("01:00AM", Time.minutesToTimeString(1 * 60));
        assertEquals("12:00AM", Time.minutesToTimeString(0));
        assertEquals("12:00PM", Time.minutesToTimeString(12 * 60));
        assertEquals("01:00PM", Time.minutesToTimeString(13 * 60));
        assertEquals("11:59PM", Time.minutesToTimeString(12 * 60 + 11 * 60 + 59));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testMinutesToTimeStringBoundary() {
        assertEquals("12:00 AM", Time.minutesToTimeString(12 * 60 + 12 * 60));
    }
}
