package alissonfl.trackman;



public class Event {
    private String name;
    private int duration;
    private TimeUnitEnum unit;

    public Event(String name, int duration, TimeUnitEnum unit) {
        this.name = name;
        this.duration = duration;
        this.unit = unit;
    }

    @Override
    public String toString() {
        return name + " - " + (unit == TimeUnitEnum.MINUTES ? duration : "") + unit;
    }

    public int getLengthInMinutes() {
        return unit.inMinutes(duration);
    }

    public String getName() {
        return name;
    }
}
