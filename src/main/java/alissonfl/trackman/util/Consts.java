package alissonfl.trackman.util;

public final class Consts {
    private Consts() {}    
    
    public static final int BEGIN_TIME = 540; //9PM
    public static final int AM_LENGTH = 180;
    public static final int PM_LENGTH = 240;
    public static final int LUNCH_LENGTH = 60;
    public static final int LUNCH_BEGIN_TIME = BEGIN_TIME + AM_LENGTH;
    public static final int NETW_LENGTH = 60;
    public static final int NETW_MIN_BEGIN_TIME = 960; //4PM
    public static final int MAX_EVENT_LENGTH = 240;

    public static final String LINE_BREAK = System.getProperty("line.separator");
    
}
